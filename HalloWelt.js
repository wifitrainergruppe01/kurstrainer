// Erste Versuche in HTML/CSS/JS

// let gibt es erst ab ES6 (früher nur var)
// Angabe für jsbin.com (alt): jshint esnext: true
// Angabe für jshint.com (neu): jshint esversion: 6
// jshint esversion: 6

// Neues Datum Objekt mit heutigem Datum anlegen
let d = new Date();

// Datum formatieren
let datum = d.getDate() + "." + (d.getMonth() + 1) + "." + d.getFullYear();

// Überschrift h1 mit Datum bilden
document.body.innerHTML = "<h1>Hallo Welt am " + datum + "!</h1>";

// Schriftfarbe im body einstellen
document.body.style.color = "white";

// Hintergrundfarbe im body einstellen
document.body.style.backgroundColor = "blue";

// Neuen Absatz p erzeugen und Variable p zuweisen
let p = document.createElement("p");

// Neuen Text erzeugen und Variable t zuweisen
let t = document.createTextNode("Hier beginnt die Webentwicklungsausbildung ...");

// Text in Absatz schreiben
p.appendChild(t);

// Absatz in body schreiben
document.body.appendChild(p);
